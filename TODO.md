## 20240509

### 20240509 19:59 | 개발환경구축 | 커밋을 해야만 변경된 사항을 볼 수 있다. 그래서 바로 적용하고 그 내용을 확인할 수 없다. 그렇기 때문에 윈도우의 간단한 웹 서버를 동작시키도록 하자. | OK

도커를 이용하여 NGINX 웹 서버를 동작시키자.

```sh
$ docker run --rm -p 80:80 --name nginx -v .\public:/usr/share/nginx/html -d nginx
```

### 20240509 21:22 | FAVICON.ICO | 아래와 같이 Not Found 404 에러가 발생한다. 파비콘을 만들자. | OK

GET http://localhost/favicon.ico 404 (Not Found)

https://developers.google.com/search/docs/appearance/favicon-in-search?hl=ko#guidelines
https://ko.wix.com/blog/post/what-is-favicon-how-to-make

- 16x16: 브라우저용
- 32x32: 작업표시줄 단축키용
- 96x96: 데스크탑 단축키용
- 180x180: 애플 터치용


| prefix                | xs          | sm       | md       | lg       | xl       | xxl       |
| --------------------- | ----------- | -------- | -------- | -------- | -------- | --------- |
| -                     | <576px      | ≥576px   | ≥768px   | ≥992px   | ≥1200px  | ≥1400px   |
| Container `max-width` | None (auto) | 540px    | 720px    | 960px    | 1140px   | 1320px    |
| Class prefix          | .col-       | .col-sm- | .col-md- | .col-lg- | .col-xl- | .col-xxl- |

- `#` of columns    12
- Gutter width      1.5rem (.75rem on left and right)
- Custom gutters    Yes
- Nestable          Yes
- Column ordering   Yes

### 20240510 14:24 | GRID SYSTEM | 필요한 그리드 시스템을 정의하자. | ING

### 20240510 16:33 | HEADER, FOOTER

### 20240510 17:38 | CONTENT

Markdown 으로 부터 정적 웹페이지를 생성하도록 하자.
점점 커지는 군!

### 20240510 | snorlax.in 도메인과 홈페이지를 연결하기

